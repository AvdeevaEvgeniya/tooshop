var gulp         = require('gulp');
var browserSync  = require('browser-sync').create();
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS     = require('gulp-clean-css');
var pug          = require('gulp-pug');
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: "dist/"
    });
    gulp.watch("src/**/*.sass", ['sass']).on('change', browserSync.reload);
    gulp.watch("src/**/*.pug", ['pug']).on('change', browserSync.reload);
    gulp.watch("src/**/*.html").on('change', browserSync.reload);
});
gulp.task('sass', function() {
    return gulp.src("src/styles/pages/**/*.sass")
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
});

gulp.task('pug', function buildHTML() {
    return gulp.src('src/pages/*/*.pug')
        .pipe(pug())
        .pipe(gulp.dest('dist/html'))
});


gulp.task('default', ['serve', 'sass', 'pug']);

//gulp default
