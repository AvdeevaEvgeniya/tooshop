'use strict';

const addEventChangeGridView = () => {
    const productList = document.querySelectorAll(".catalog__list"),
        viewGrid = document.querySelectorAll(".view-grid"),
        viewList = document.querySelectorAll(".view-list"),
        len = document.querySelectorAll(".catalog__list").length;
    console.log(len);
    for(let i = 0; i < len; i++) {
        viewGrid[i].onchange = () => productList[i].classList.remove("catalog__list_grid");
        viewList[i].onchange = () => productList[i].classList.add("catalog__list_grid");
    }
};

addEventChangeGridView();